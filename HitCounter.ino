// Common CATHODE display
#include <EEPROM.h>
#include "display/SegmentDisplay.h"
#include "lib/MemoryOperations.h"
#include "lib/Logger.h"
#include "Variables.h"

DisplayElements elements(pinA, pinB, pinC, pinD, pinE, pinF, pinG, P1, P2, P3, P4);
SegmentDisplay display(elements);
int storedRecord = 0;
int currentCount = 0;
Phase nextPhase = COUNTING_CYCLE;

void startup();
void mainCountingCycle();
void currentCountAndRecord();

void setup()
{
  pinMode(pinA, OUTPUT);
  pinMode(pinB, OUTPUT);
  pinMode(pinC, OUTPUT);
  pinMode(pinD, OUTPUT);
  pinMode(pinE, OUTPUT);
  pinMode(pinF, OUTPUT);
  pinMode(pinG, OUTPUT);
  pinMode(P1, OUTPUT);
  pinMode(P2, OUTPUT);
  pinMode(P3, OUTPUT);
  pinMode(P4, OUTPUT);

  pinMode(pinSensor, INPUT);

  Serial.begin(9600);
  EEPROM_initialize();

  // Read stored record from memory
  // EEPROM_updateRecordIfHigher(4); //DEBUG

  storedRecord = EEPROM_readRecord();
  if (storedRecord != 0)
  {
    lastMillis = millis();
    nextPhase = STARTUP;
  }
  else
  {
    nextPhase = COUNTING_CYCLE;
  }
}

void loop()
{
  switch (nextPhase)
  {
  case STARTUP:
    startup();
    break;
  case COUNTING_CYCLE:
    mainCountingCycle();
    break;
  case CORRECT_CYCLE:
    currentCountAndRecord();
    break;
  case STORE_RECORD:
    EEPROM_updateRecordIfHigher(currentCount);
    storedRecord = EEPROM_readRecord();
    nextPhase = CORRECT_CYCLE;
    break;
  case ERROR:
  default:
    display.dash(4);
    break;
  }
}

// Running before counting cycle for 5 seconds
// If showStoredRecord is true
void startup()
{
  display.show(storedRecord);
  if ((millis() - lastMillis) >= 5000)
  {
    nextPhase = COUNTING_CYCLE;
  }
}

// Main counting cycle
// Running if countingCycle == true
// Counting hits and showing current count
void mainCountingCycle()
{
  if (digitalRead(pinSensor) == HIGH && previousRead == LOW)
  {
    previousRead = HIGH;
  }
  if (digitalRead(pinSensor) == LOW && previousRead == HIGH)
  {
    if (firstHit || everyHit)
    {
      currentCount++;
      counting = true;
      lastHitMillis = millis();
    }
    firstHit = !firstHit;
    previousRead = LOW;
    if (currentCount == 1)
      lastMillis = millis();
  }

  display.show(currentCount);

  if (counting)
  {
    if ((millis() - lastHitMillis >= 7500))
    {
      nextPhase = ERROR;
      counting = false;
    }
    if (millis() - lastMillis >= 1000u * cycleLengthSeconds)
    {
      nextPhase = STORE_RECORD;
      counting = false;
    }
  }
}

// Running after countingCycle
// Displaying current record and previous record
void currentCountAndRecord()
{
  if (millis() - lastMillis <= 500)
  {
    if (!displayOff)
    {
      display.turnOff();
      displayOff = true;
    }
  }
  else if (millis() - lastMillis > 500 &&
           millis() - lastMillis <= 2000)
  {
    display.show(currentCount);
    displayOff = false;
  }
  else if (millis() - lastMillis > 2000 &&
           millis() - lastMillis <= 3500)
  {
    display.show(storedRecord);
    displayOff = false;
  }
  else
  {
    lastMillis = millis();
  }
}