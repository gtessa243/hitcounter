#ifndef VARIABLES_H
#define VARIABLES_H

const char pinA = 2;
const char pinB = 3;
const char pinC = 4;
const char pinD = 5;
const char pinE = 6;
const char pinF = 7;
const char pinG = 8;
const char P1 = 9;
const char P2 = 10;
const char P3 = 11;
const char P4 = 12;

const char pinSensor = 13;

char cycleLengthSeconds = 60;
int previousRead = HIGH;
unsigned long lastMillis = 0;
unsigned long lastHitMillis = 0; // millis of the last hit
boolean everyHit = false;        // count every hit?
boolean firstHit = true;         // is this first hit? (not the bounceback)
boolean correctCycle = true;     // correct cycle ends after configured length
boolean countingCycle = false;   // should code be in counting cycle?
boolean counting = false;        // should count already? (waiting for the first hit)
boolean displayOff = false;      // for blinking
boolean showStoredRecord = false;

const int mem_multiplierAddress = 10;
const int mem_moduloAddress = 11;
boolean debug = false;

enum Phase
{
    STARTUP,
    SHOW_RECORD,
    COUNTING_CYCLE,
    CORRECT_CYCLE,
    STORE_RECORD,
    ERROR
};

#endif VARIABLES_H