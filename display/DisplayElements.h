#ifndef DisplayElements_h
#define DisplayElements_h

#include <Arduino.h>

class DisplayElements
{
public:
    DisplayElements() = default;
    DisplayElements(int A, int B, int C, int D, int E, int F, int G, int P1, int P2, int P3, int P4)
    {
        _pinA = A;
        _pinB = B;
        _pinC = C;
        _pinD = D;
        _pinE = E;
        _pinF = F;
        _pinG = G;
        _P1 = P1;
        _P2 = P2;
        _P3 = P3;
        _P4 = P4;
    };
    void dash(int pos);
    void one(int pos);
    void two(int pos);
    void three(int pos);
    void four(int pos);
    void five(int pos);
    void six(int pos);
    void seven(int pos);
    void eight(int pos);
    void nine(int pos);
    void zero(int pos);
    void blank();

private:
    void choosePosition(int pos);
    int _pinA;
    int _pinB;
    int _pinC;
    int _pinD;
    int _pinE;
    int _pinF;
    int _pinG;
    int _P1;
    int _P2;
    int _P3;
    int _P4;
};

void DisplayElements::blank() {
    digitalWrite(_P4, HIGH);
    digitalWrite(_P3, HIGH);
    digitalWrite(_P2, HIGH);
    digitalWrite(_P1, HIGH);
    digitalWrite(_pinA, LOW);
    digitalWrite(_pinB, LOW);
    digitalWrite(_pinC, LOW);
    digitalWrite(_pinD, LOW);
    digitalWrite(_pinE, LOW);
    digitalWrite(_pinF, LOW);
    digitalWrite(_pinG, LOW);
}

void DisplayElements::choosePosition(int pos)
{
    digitalWrite(_P1, HIGH);
    digitalWrite(_P2, HIGH);
    digitalWrite(_P3, HIGH);
    digitalWrite(_P4, HIGH);

    switch (pos)
    {
    case 1:
        digitalWrite(_P1, LOW);
        break;
    case 2:
        digitalWrite(_P2, LOW);
        break;
    case 3:
        digitalWrite(_P3, LOW);
        break;
    case 4:
        digitalWrite(_P4, LOW);
        break;
    }
}

void DisplayElements::one(int pos)
{
    choosePosition(pos);
    digitalWrite(_pinA, LOW);
    digitalWrite(_pinB, HIGH);
    digitalWrite(_pinC, HIGH);
    digitalWrite(_pinD, LOW);
    digitalWrite(_pinE, LOW);
    digitalWrite(_pinF, LOW);
    digitalWrite(_pinG, LOW);
    delay(5);
}

void DisplayElements::two(int pos)
{
    choosePosition(pos);
    digitalWrite(_pinA, HIGH);
    digitalWrite(_pinB, HIGH);
    digitalWrite(_pinC, LOW);
    digitalWrite(_pinD, HIGH);
    digitalWrite(_pinE, HIGH);
    digitalWrite(_pinF, LOW);
    digitalWrite(_pinG, HIGH);
    delay(5);
}

void DisplayElements::three(int pos)
{
    choosePosition(pos);
    digitalWrite(_pinA, HIGH);
    digitalWrite(_pinB, HIGH);
    digitalWrite(_pinC, HIGH);
    digitalWrite(_pinD, HIGH);
    digitalWrite(_pinE, LOW);
    digitalWrite(_pinF, LOW);
    digitalWrite(_pinG, HIGH);
    delay(5);
}

void DisplayElements::four(int pos)
{
    choosePosition(pos);
    digitalWrite(_pinA, LOW);
    digitalWrite(_pinB, HIGH);
    digitalWrite(_pinC, HIGH);
    digitalWrite(_pinD, LOW);
    digitalWrite(_pinE, LOW);
    digitalWrite(_pinF, HIGH);
    digitalWrite(_pinG, HIGH);
    delay(5);
}

void DisplayElements::five(int pos)
{
    choosePosition(pos);
    digitalWrite(_pinA, HIGH);
    digitalWrite(_pinB, LOW);
    digitalWrite(_pinC, HIGH);
    digitalWrite(_pinD, HIGH);
    digitalWrite(_pinE, LOW);
    digitalWrite(_pinF, HIGH);
    digitalWrite(_pinG, HIGH);
    delay(5);
}

void DisplayElements::six(int pos)
{
    choosePosition(pos);
    digitalWrite(_pinA, HIGH);
    digitalWrite(_pinB, LOW);
    digitalWrite(_pinC, HIGH);
    digitalWrite(_pinD, HIGH);
    digitalWrite(_pinE, HIGH);
    digitalWrite(_pinF, HIGH);
    digitalWrite(_pinG, HIGH);
    delay(5);
}

void DisplayElements::seven(int pos)
{
    choosePosition(pos);
    digitalWrite(_pinA, HIGH);
    digitalWrite(_pinB, HIGH);
    digitalWrite(_pinC, HIGH);
    digitalWrite(_pinD, LOW);
    digitalWrite(_pinE, LOW);
    digitalWrite(_pinF, LOW);
    digitalWrite(_pinG, LOW);
    delay(5);
}

void DisplayElements::eight(int pos)
{
    choosePosition(pos);
    digitalWrite(_pinA, HIGH);
    digitalWrite(_pinB, HIGH);
    digitalWrite(_pinC, HIGH);
    digitalWrite(_pinD, HIGH);
    digitalWrite(_pinE, HIGH);
    digitalWrite(_pinF, HIGH);
    digitalWrite(_pinG, HIGH);
    delay(5);
}

void DisplayElements::nine(int pos)
{
    choosePosition(pos);
    digitalWrite(_pinA, HIGH);
    digitalWrite(_pinB, HIGH);
    digitalWrite(_pinC, HIGH);
    digitalWrite(_pinD, HIGH);
    digitalWrite(_pinE, LOW);
    digitalWrite(_pinF, HIGH);
    digitalWrite(_pinG, HIGH);
    delay(5);
}

void DisplayElements::zero(int pos)
{
    choosePosition(pos);
    digitalWrite(_pinA, HIGH);
    digitalWrite(_pinB, HIGH);
    digitalWrite(_pinC, HIGH);
    digitalWrite(_pinD, HIGH);
    digitalWrite(_pinE, HIGH);
    digitalWrite(_pinF, HIGH);
    digitalWrite(_pinG, LOW);
    delay(5);
}

void DisplayElements::dash(int pos)
{
    choosePosition(pos);
    digitalWrite(_pinA, LOW);
    digitalWrite(_pinB, LOW);
    digitalWrite(_pinC, LOW);
    digitalWrite(_pinD, LOW);
    digitalWrite(_pinE, LOW);
    digitalWrite(_pinF, LOW);
    digitalWrite(_pinG, HIGH);
    delay(5);
}

#endif