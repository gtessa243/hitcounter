#ifndef SegmentDisplay_h
#define SegmentDisplay_h

#include <Arduino.h>
#include "DisplayElements.h"

class SegmentDisplay
{
private:
    void show(int number, int pos);
    DisplayElements _elements;

public:
    SegmentDisplay(DisplayElements elements) {
        SegmentDisplay::_elements = elements;
    }
    void show(int number);
    void dash(int pos) { _elements.dash(pos); };
    void turnOff();
};

void SegmentDisplay::show(int number)
{
    int ones = 0, tens = 0, hunds = 0, tous;
    ones = number % 10;
    tens = ((number % 100) - ones) / 10;
    hunds = ((number % 1000) - tens) / 100;
    tous = ((number % 10000) - hunds) / 1000;
    show(ones, 4);
    if (number > 9)
        show(tens, 3);
    if (number > 99)
        show(hunds, 2);
    if (number > 999)
        show(tous, 1);
}

void SegmentDisplay::show(int number, int pos)
{
    switch (number)
    {
    case 0:
        _elements.zero(pos);
        break;
    case 1:
        _elements.one(pos);
        break;
    case 2:
        _elements.two(pos);
        break;
    case 3:
        _elements.three(pos);
        break;
    case 4:
        _elements.four(pos);
        break;
    case 5:
        _elements.five(pos);
        break;
    case 6:
        _elements.six(pos);
        break;
    case 7:
        _elements.seven(pos);
        break;
    case 8:
        _elements.eight(pos);
        break;
    case 9:
        _elements.nine(pos);
        break;
    }
}

void SegmentDisplay::turnOff()
{
    _elements.blank();
}

#endif