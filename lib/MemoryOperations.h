#ifndef MEMORYOPERATIONS_H
#define MEMORYOPERATIONS_H

#include <EEPROM.h>
#include <Arduino.h>
#include "../Variables.h"
#include "Logger.h"

typedef struct
{
    int modulo;
    int multiplier;
} recordParts;

recordParts divideRecord(int record)
{
    recordParts parts = recordParts();
    parts.multiplier = record / 255;
    parts.modulo = record % 255;
    return parts;
}

template <class T>
int EEPROM_writeAnything(int ee, const T &value)
{
    const byte *p = (const byte *)(const void *)&value;
    unsigned int i;
    for (i = 0; i < sizeof(value); i++)
        EEPROM.write(ee++, *p++);
    return i;
}

template <class T>
int EEPROM_readAnything(int ee, T &value)
{
    byte *p = (byte *)(void *)&value;
    unsigned int i;
    for (i = 0; i < sizeof(value); i++)
        *p++ = EEPROM.read(ee++);
    return i;
}

int EEPROM_readRecord()
{
    recordParts memory = recordParts();
    memory.multiplier = EEPROM.read(mem_multiplierAddress);
    memory.modulo = EEPROM.read(mem_moduloAddress);
    log("Memory multiplier: ", memory.multiplier);
    log("Memory modulo: ", memory.modulo);
    int mem_record = (memory.multiplier * 255) + memory.modulo;
    log("Record from memory: ", mem_record);
    return mem_record;
}

void EEPROM_updateRecordIfHigher(int currentRecord)
{
    int mem_record = EEPROM_readRecord();
    log("Current record: ", currentRecord);
    if (currentRecord > mem_record)
    {
        recordParts current = divideRecord(currentRecord);
        log("Storing new record");
        EEPROM.update(mem_multiplierAddress, current.multiplier);
        EEPROM.update(mem_moduloAddress, current.modulo);
    }
}

void EEPROM_initialize()
{
    if ((EEPROM.read(10) % 255 == 0) &&
        EEPROM.read(11) % 255 == 0)
    {
        EEPROM.update(10, 0);
        EEPROM.update(11, 0);
    }
}

#endif MEMORYOPERATIONS_H