#ifndef LOGGER_H
#define LOGGER_H

#include "../Variables.h"

void log(String message, int number)
{
    if (debug)
    {
        Serial.print(message);
        Serial.println(number);
    }
}

void log(String message)
{
    if (debug)
    {
        Serial.println(message);
    }
}

#endif LOGGER_H